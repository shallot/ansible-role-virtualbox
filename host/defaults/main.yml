# Copyright (c) 2019-2023 eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---

# The public bits of the keys Oracle signes the APT packages with.
virtualbox_host_apt_keys:
  oracle_vbox_2016:
    data: "{{ lookup('file', 'oracle_vbox_2016.asc') }}"
    state: "present"
  oracle_vbox:
    data: "{{ lookup('file', 'oracle_vbox.asc') }}"
    state: "present"

# The path to the VirtualBox APT source list file on Debian systems.
virtualbox_host_apt_repository_file:
  "/etc/apt/sources.list.d/virtualbox.list"

# The content of the VirtualBox APT source list file on Debian systems.
# Note that at the time of writing there was no Debian "buster" specific
# package available (yet?), hence the current workaround using the package
# for Ubuntu "bionic" instead.
virtualbox_host_apt_repository_record: |
  # Managed by Ansible
  deb https://download.virtualbox.org/virtualbox/debian {{
    (ansible_distribution_release == 'buster')
    | ternary('bionic', ansible_distribution_release)
  }} contrib

# The name of the VirtualBox package.
virtualbox_host_package_name:
  "virtualbox-6.0"

# The target state of the VirtualBox package.
virtualbox_host_package_state:
  "present"
